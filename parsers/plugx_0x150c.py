import sys, string
from io import StringIO
from struct import unpack_from, calcsize, unpack, pack
from socket import inet_ntoa

from structs.plugx_structs import get_proto, PlugX_0x2d58, persistence, reg_hives
from common.abstracts import MalParserBase
from lib.validator import is_valid_host, is_valid_port

class MalParser(MalParserBase):
	def __init__(self):
		super(MalParser, self).__init__()
		self.cfg_structs = PlugX_0x2d58
		self.cfg_size = 0x2d58
		self.cfg_start_offset = -80 
		self.magic = b'\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01' 
		self.raw_cfg = None
		self.pretty_cfg = ''

	def make_print_format(self, cfg):
		outfd = StringIO()

		for cfg_field, cfg_type in cfg._fields_:
			if cfg_field.startswith(('unused','net_access')): continue
			cfg_value = getattr(cfg,cfg_field)
			if cfg_field=='mac_disable':
				b = [x for x in cfg_value]
				mac_str = '{:02X}:{:02X}:{:02X}:{:02X}:{:02X}:{:02X}'.format(*b)
				outfd.write("{}: {}\n".format(cfg_field.replace('_',' ').title(),mac_str))
			elif getattr(cfg_value,'_type_',None):
				for x in cfg_value:
					if cfg_field == 'cnc':
						if x.host:
							outfd.write("{}: {}:{} ({})\n".format(cfg_field.replace('_',' ').title(),x.host.decode('utf-8'),x.port,get_proto(x.proto)))
					elif cfg_field == 'proxy':
						if x.host:
							outfd.write("{}: {}:{} ({} / {})\n".format(cfg_field.replace('_',' ').title(),x.host.decode('utf-8'),x.port,x.user,x.passwd))
					elif cfg_field == 'dns':
						if x:
							outfd.write("{}: {}\n".format(cfg_field.replace('_',' ').title(),inet_ntoa(pack("<I",x))))
					elif len(getattr(x,'_fields_',[])) > 1:
						a = {}
						for y,z in x._fields_:
							if getattr(x,y):
								outfd.write("{} {}: {}\n".format(cfg_field.replace('_',' ').title(),y.replace('_',' ').title(),y))
					else:
						if str(x):
							outfd.write('{}: {}\n'.format(cfg_field.replace('_',' ').title(),x))
			elif cfg_field == 'persistence':
				outfd.write('{}: {}\n'.format(cfg_field.replace('_',' ').title(),persistence[cfg_value]))
			elif cfg_field == 'reg_hive':
				outfd.write('{}: {}\n'.format(cfg_field.replace('_',' ').title(),reg_hives.get(cfg_value,"Unknown")))
			elif 'end_scan' in cfg_field or 'start_scan' in cfg_field:
				outfd.write('{}: {}\n'.format(cfg_field.replace('_',' ').title(),inet_ntoa(pack("<I",cfg_value))))
			else:
				if str(cfg_value):
					outfd.write('{}: {}\n'.format(cfg_field.replace('_',' ').title(),cfg_value))

		self.pretty_cfg = outfd.getvalue()
		outfd.close()

		return


	def validate(self, cfg):
		cnc_array = getattr(cfg, "cnc")
		for cnc in cnc_array:
			str_cnc = ""
			try:
				str_cnc = cnc.host.decode("utf-8")
			except UnicodeDecodeError as e:
				return False
			if is_valid_host(str_cnc) and is_valid_port(cnc.port):
				return True
		return False


if __name__ == '__main__':

	if len(sys.argv) < 2:
		print ("Syntex : \n%s filename" % sys.argv[0])
		exit()

	blob = None
	with open(sys.argv[1], 'rb') as f:
		blob = f.read()
	
	p = MalParser()
	p.mem_parse(blob)
	if not p.raw_cfg:
		print("Decode failed!")
	print(p.pretty_cfg)
