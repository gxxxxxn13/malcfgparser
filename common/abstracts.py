

class MalParserBase(object):
	def __init__(self):
		self.cfg_structs = None
		self.magic = None
		self.cfg_size = None
		self.cfg_start_offset = None # offset of cfg start to magic 
		self.try_decode = None
		self.raw_cfg = None
		self.pretty_cfg = None

	def __find_magic(self, memblock):
		index = memblock.find(self.magic)
		return index

	def __parse(self, cfg_blob):
		cfg = self.cfg_structs.from_buffer_copy(cfg_blob)
		return cfg

	def cfg_parse(self, config_blob):
		# auto brute force
		for i in range(len(config_blob)+1-self.cfg_size):
			if (self.cfg_size+i) > len(config_blob):
				break
			trim_config_blob = config_blob[i:self.cfg_size+i]

			if self.try_decode:
				trim_config_blob = self.cfg_decode(trim_config_blob)

			cfg = self.__parse(trim_config_blob)
			
			if self.validate(cfg):
				self.raw_cfg = cfg
				break

		if self.raw_cfg:
			self.make_print_format(self.raw_cfg)

	def mem_parse(self, memblock):
		index = self.__find_magic(memblock)
		if index == -1:
			return False
		cfg_start_try = index+self.cfg_start_offset
		if cfg_start_try < 0:
			cfg_start_try = 0
		self.cfg_parse(memblock[cfg_start_try:])

	def make_print_format(self, cfg):
		raise NotImplementedError()

	def validate(self, cfg):
		raise NotImplementedError()

	def cfg_decode(self, blob):
		if self.try_decode:
			raise NotImplementedError()
		pass