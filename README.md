# MalCfgParser

```
___  ___      _ _____  __     ______                        
|  \/  |     | /  __ \/ _|    | ___ \                       
| .  . | __ _| | /  \/ |_ __ _| |_/ /_ _ _ __ ___  ___ _ __ 
| |\/| |/ _` | | |   |  _/ _` |  __/ _` | '__/ __|/ _ \ '__|
| |  | | (_| | | \__/\ || (_| | | | (_| | |  \__ \  __/ |   
\_|  |_/\__,_|_|\____/_| \__, \_|  \__,_|_|  |___/\___|_|   
                          __/ |                             
                         |___/                              
```

MalCfgParser is a malware configuration parser that:

- Brute-forcely parses memory -- No need to decode and specify the configuration size!
- Accepts the PID or use dump files -- No more whole disk dump in Volatility!
- Easy to implement your parser by adding yara and malware configuration structs

## Requirements
### Local machine
- VMWare
- python3
- yara-python
> For Windows, the installers are put under `requirements`

### Remote machine: Windows7 on VMware
- python (Any version is OK)

## Configuration
In default.cfg, set up:
```
vmrun=<Path to vmrun>
vmx=<Path to vmx file of remote machine>
vm_user=<Username of remote machine>
vm_password=<Password of remote machine>
work_folder=<Workspace in remote machine, work_folder=C:\MalCfgParser by default>
dump_files_folder=<Folder to save the memory dump files in local machine>
```

## Usage
### Parse by PID in running machine
```
> python3 main.py <pid>
```

Example: 
```
> python3 main.py 6264
[+] work_folder C:\MalCfgParser was already in VM
[+] memdumper.py is transmitted to VM
[+] Memory dump 6264 is OK in VM
[+] Memory dump files from VM are retreived
[+] Detect: cobaltstrike
BeaconType: 8 (HTTPS)
Port: 443
Polling_ms: 60000
Unknown1: \0x00\0x10\0x00\0x00
Jitter: 0
...
```

### Parse by memory dump file
```
> python3 main.py <memory dump file>
```

Example:
```
> python3 main.py test/malware/plugx_0x2d58/memdump/iexplore.exe_0x300000-0x2a000.bin
[+] Detect: plugx_0x2d58
Hide Dll: 0
Delete Self: 0
Keylogger: 0
Sleep1: 167772160
Sleep2: 0
Cnc: www.microsoftupgrade.instanthq.com:80 (HTTP)
Cnc: www.microsoftupgrade.instanthq.com:80 (HTTP)
Cnc: www.microsoftupgrade.instanthq.com:443 (HTTP)
Cnc: www.microsoftupgrade.instanthq.com:53 (HTTP)
Persistence: None
Install Folder: %AUTO%\xs
...
```

## Credit
The design of malware structure is reference from the following script:
- https://github.com/jjo-sec/volatility_plugins