from ctypes import *
import re
from collections import defaultdict

persistence = defaultdict(lambda: "Unknown", {0: "Service + Run Key", 1: "Service", 2: "Run key", 3: "None"})

reg_hives = {
        0x80000002 : 'HKLM',
        0x80000001 : 'HKCU',
        0x80000000 : 'HKCR',
        0x80000003 : 'HKU',
        0x80000004 : 'HKPD',
        0x80000050 : 'HKPT',
        0x80000060 : 'HKPN',
        0x80000005 : 'HKCC',
        0x80000006 : 'HKDD',
    }

def get_proto(proto):
    ret = []
    if proto & 0x1:
        ret.append("TCP")
    if proto & 0x2:
        ret.append("HTTP")
    if proto & 0x4:
        ret.append("UDP")
    if proto & 0x8:
        ret.append("ICMP")
    if proto & 0x10:
        ret.append("DNS")
    if proto > 0x1f:
        ret.append("OTHER_UNKNOWN")
    return ' / '.join(ret)

class PlugX_0x150c(Structure):
    _fields_ = [("Flags",c_ubyte*10),
                ("Timer1",c_ubyte*4),
                ("Timer2",c_ubyte*4),
                ("CnC1",c_char*44)
            ]
